#!/bin/sh -e

VERSION=$2
TAR=../findbugs-bcel_$VERSION.orig.tar.gz
DIR=findbugs-bcel-$VERSION
TAG=$(echo "findbugs-bcel-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export http://svn.apache.org/repos/asf/commons/proper/bcel/tags/${TAG} $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
